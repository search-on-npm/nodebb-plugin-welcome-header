"use strict";

const HeaderHelpers = require("./helpers");
let Controllers = {};

Controllers.renderAdmin = async (req, res) => {
  const headers = await HeaderHelpers.getAllHeaders();

  res.render("admin/plugins/welcome-header", {
    items: headers || [],
  });
};

module.exports = Controllers;
