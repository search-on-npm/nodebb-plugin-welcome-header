"use strict";

const db = require.main.require("./src/database");
const utils = require.main.require("./src/utils");

const Helpers = {};

const HEADER_SET_KEY = "connect-welcome-header";

Helpers.getAllHeaders = async () => {
  const uuids = await db.getSortedSetRange(HEADER_SET_KEY, 0, -1);
  const headers = await Promise.all(
    uuids.map((uuid) => db.getObject(`${HEADER_SET_KEY}:whid:${uuid}`))
  );
  return headers;
};

Helpers.saveHeader = async (data) => {
  if (!data.whid || data.whid == "") {
    data.whid = utils.generateUUID();
    await db.sortedSetAdd(HEADER_SET_KEY, Date.now(), data.whid);
  }

  await db.setObject(`${HEADER_SET_KEY}:whid:${data.whid}`, data);

  return data;
};

Helpers.getHeader = async (whid) => {
  if (!whid) throw new Error("No id provided. Cannot fetch header.");
  return await db.getObject(`${HEADER_SET_KEY}:whid:${whid}`);
};

Helpers.deleteHeader = async (whid) => {
  await db.sortedSetRemove(HEADER_SET_KEY, whid);
  await db.delete(`${HEADER_SET_KEY}:whid:${whid}`);
  return { message: "Welcome Header deleted" };
};

Helpers.isValidMedalsObject = (medal) => {
  if (
    !medal.name ||
    !medal.description ||
    (!medal.icon && !medal.customIcon) ||
    !medal.addedByUid
  ) {
    return false;
  }
  return true;
};

module.exports = Helpers;
