"use strict";

const db = require.main.require("./src/database");
const meta = require.main.require("./src/meta");

var Sockets = {};

Sockets.groups = async () => {
  const groups = await db.getSortedSetRevRange("groups:createtime", 0, -1);
  return groups.filter(function (name) {
    return name.indexOf(":privileges:") === -1 && name !== "registered-users";
  });
};

Sockets.readed = async (socket, data) => {
  var set = "connect-welcome-header";
  if (!data || !data.whid) {
    return { message: "Si è verificato un errore grave cod.65791" };
  }
  await db.listAppend(set + ":whid:uid:" + socket.uid + ":readed", data.whid);
  return { message: "Welcome Header readed added" };
};

module.exports = Sockets;
