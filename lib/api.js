"use strict";

const HeaderHelpers = require("./helpers");
const controllerHelpers = require.main.require("./src/controllers/helpers");

const API = {};

API.getHeaders = async () => {
  const medals = await HeaderHelpers.getAllHeaders();
  return medals;
};

API.getHeader = async (req, res) => {
  const medal = await HeaderHelpers.getHeader(req.params.whid);
  controllerHelpers.formatApiResponse(200, res, { medal: medal });
};

API.deleteHeader = async (req, res) => {
  try {
    const { uuid } = req.body;

    if (!uuid) throw new Error("No uuid provided");

    await HeaderHelpers.deleteHeader(uuid);

    controllerHelpers.formatApiResponse(200, res);
  } catch (error) {
    throw new Error(error.message);
  }
};

API.saveHeader = async (req, res) => {
  try {
    const { header } = req.body;

    const savedHeader = await HeaderHelpers.saveHeader(header);

    controllerHelpers.formatApiResponse(200, res, {
      header: savedHeader,
    });
  } catch (error) {
    throw new Error(error.message);
  }
};

module.exports = API;
