# Welcome Header per Connect

**[FUNZIONALITA']**<br />

Questo plugin permette, lato admin, di inserire un determinato messaggio che può essere visualizzato da un particolare target di utenti.
Di seguito viene mostrata come si presenta la pagina di questo plugin:<br />
 


![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-welcome-header/raw/demaoui/screenshot/connect-welcome-header-admin-page.png)<br />

Cliccando sul pulsante a destra "+" comparirà una finestra in cui si possono inserire diversi parametri per creare un welcome header. La finestra che si verrà a creare sarà divisa in "Main" e "Condition" dove:<br />

* **Main**: in cui si specificheranno tutti i parametri relativi al nome del welcome header oppure il colore,etc.
* **Conditions**:  in cui si specificheranno le condizioni che si dovranno verificare affinchè il welcome header sia visibile

<br/>
Di seguito è mostrato come comparirà la finestra che permette di specificare i parametri della parte "Main":

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-welcome-header/raw/demaoui/screenshot/connect-welcome-header-main.png)<br />


* **Name**: si inserisce il nome del welcome header
* **Text**: specifica il testo che deve comparire agli utenti
* **Background Color**: specifica il colore di un annuncio che deve essere nel formato HEX	(es. #ffffff)
* **Text Color**: permette di specificare il colore del testo
* **Active**: permette di attivare il welcome header altrimenti non verrà visualizzato
* **Persistent**: rende il messaggio non cancellabile
* **Hidable**: il messaggio può essere cancellato dall'utente una volta visualizzato.

<br />

Una volta inseriti i parametri principali è d'obbligo inserire anche le condizioni affinchè il welcome header sia visibile: <br />

![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-welcome-header/raw/demaoui/screenshot/connect-welcome-header-conditions.png)<br />

Ci sono diverse condizioni che possono essere settate affinchè il welcome header sia visibile all'utente e in particolare: <br/>

* **L'utente appartiene ai gruppi**: permette di specificare a quali gruppi deve appartenere l'utente affinchè il welcome header venga visualizzato
* **L'utente non appartiene ai gruppi**: permette di specificare a quali gruppi l'utente _non_ appartiene
* **L'utente ha scritto un numero di post tra**: permette di specificare gli utenti che hanno scritto un numero di post compreso tra il numero minimo e il numero massimo specificati dall'admin
* **L'utente è**: permette di specificare l'username di un utente
* **L'utente non è loggato**: permette di visualizzare il welcome header solo agli utente non loggati

<br />
Nell'esempio precedente si era scelto di mostrare il welcome header solo agli utenti non loggati. Quindi una volta inseriti tutti i parametri e le condizioni e dopo aver salvato, il welcome header sarà visualizzato in questa maniera:


![alt text](https://gitlab.com/gt-connect/nodebb-plugin-connect-welcome-header/raw/demaoui/screenshot/connect-welcome-header-risultato.png)<br />





         