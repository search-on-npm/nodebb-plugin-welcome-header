"use strict";

function welcomeHeaderClose(whid) {
	socket.emit('plugins.connectwelcomeheader.readed', {
		whid: whid,
	}, function(err) {
		if (err) {
			app.alertError(err);
		}
	});
}