"use strict";

define("admin/plugins/welcome-header", [
  "components",
  "alerts",
  "api",
  "bootbox",
], function (components, alerts, api, bootbox) {
  var ACP = {};

  ACP.init = function () {
    components
      .get("nodebb-plugin-welcome-header/add-header-btn")
      .off("click")
      .on("click", () => {
        components
          .get("nodebb-plugin-welcome-header/add-header-btn")
          .prop("disabled", true);

        loadModal();
      });

    document.querySelectorAll(".item_update").forEach((element) => {
      element.addEventListener("click", function () {
        api.get(
          "/plugins/welcome-header/" + element.dataset.id,
          {},
          (err, response) => {
            if (err) {
              alerts.error(err.message, 2500);
              console.error(err);
              return;
            }
            loadModal(response.medal);
          }
        );
      });
    });

    document.querySelectorAll(".item_delete").forEach((element) => {
      element.addEventListener("click", function () {
        let key = element.dataset.id;
        api.delete(
          "/plugins/welcome-header/",
          { uuid: key },
          (err, response) => {
            if (err) {
              alerts.error(err.message, 2500);
              console.error(err);
              return;
            }
            alerts.success("Header deleted", 2500);
            ajaxify.refresh();
          }
        );
      });
    });

    function loadModal(values) {
      if (!values) {
        values = {};
      }

      socket.emit(
        "plugins.connectwelcomeheader.groups",
        {},
        function (err, groups) {
          if (err) {
            app.alertError(err);
          }
          app.parseAndTranslate(
            "admin/modals/welcome-header",
            {
              groups: groups,
            },
            function (tpl) {
              var modal = bootbox.dialog({
                message: tpl,
                title: "Creazione Welcome Header",
                buttons: {
                  success: {
                    label: "Save",
                    className: "btn-primary welcome-header-save",
                    callback: function () {
                      return saveFields();
                    },
                  },
                },
              });
              modal.on("shown.bs.modal", function () {
                loadForm($("#connect_welcome_header_frm"), values);
              });
            }
          );
        }
      );
    }

    function serializeForm(formEl) {
      const values = formEl.serializeObject();

      // "Fix" checkbox values, so that unchecked options are not omitted
      formEl.find('input[type="checkbox"]').each(function (idx, inputEl) {
        inputEl = $(inputEl);
        if (!inputEl.is(":checked")) {
          values[inputEl.attr("name")] = false;
        } else {
          values[inputEl.attr("name")] = true;
        }
      });

      // save multiple selects as json arrays
      formEl.find("select[multiple]").each(function (idx, selectEl) {
        selectEl = $(selectEl);
        values[selectEl.attr("name")] = JSON.stringify(selectEl.val());
      });

      return values;
    }

    function loadForm(formEl, values) {
      $(formEl)
        .find("select[multiple]")
        .each(function (idx, selectEl) {
          const key = $(selectEl).attr("name");
          if (key && values.hasOwnProperty(key)) {
            try {
              values[key] = JSON.parse(values[key]);
            } catch (e) {
              // Leave the value as is
            }
          }
        });

      $(formEl).deserialize(values);
      $(formEl)
        .find('input[type="checkbox"]')
        .each(function (idx, inputEl) {
          inputEl = $(inputEl);

          inputEl.prop("checked", values[inputEl.attr("name")]);

          $(this)
            .parents(".mdl-switch")
            .toggleClass("is-checked", $(this).is(":checked"));
        });
    }

    function saveFields() {
      $(".welcome-header-save").prop("disabled", true);
      const formValues = serializeForm($("#connect_welcome_header_frm"));

      api.put(
        "/plugins//welcome-header",
        { header: formValues },
        (err, response) => {
          if (err) {
            alerts.error(err.message, 2500);
            $(".welcome-header-save").prop("disabled", false);
            console.error(err);
            return;
          }
          $(".welcome-header-save").prop("disabled", false);
          alerts.success("Header saved", 2500);
          ajaxify.refresh();
        }
      );
    }
  };

  return ACP;
});
