<div>
  <div class="alert alert-info row">
    <div class="col-md-6">
      <h4>Active:</h4>
      <p>Indica se l'header è attivo</p>
    </div>
    <div class="col-md-6">
      <h4>Persistent:</h4>
      <p>
        Scegli se l'header verrà sempre visualizzato.<br />Altrimenti verrà
        visualizzato solo una volta (una impression)
      </p>
    </div>
    <div class="col-md-6">
      <h4>Hide:</h4>
      <p>
        Può essere nascosto dall'utente.<br />Utilizzato da solo non ha molto
        senso. Utilizzare con <i>Persistent</i>.
      </p>
    </div>
    <div class="col-md-6">
      <h4>Persistent + Hide:</h4>
      <p>
        L'header verrà sempre visualizzato fino a quando non verrà nascosto
        dall'utente.
      </p>
    </div>
  </div>
  <div class="post-container" data-next="{next}">
    <div class="categories">
      <!-- IF !items.length -->
      <div class="text-center">Nessun Welcome Header inserito</div>
      <!-- ENDIF !items.length -->
      <ul id="NPCWH_items">
        <!-- BEGIN items -->
        <li class="item" data-whid="{items.whid}">
          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-md-3 text-center">
                      <span
                        class="label label-<!-- IF items.active -->success<!-- ELSE -->danger<!-- ENDIF items.active -->"
                        >Active</span
                      >
                      <span
                        class="label label-<!-- IF items.persistent -->primary<!-- ELSE -->default<!-- ENDIF items.persistent -->"
                        >Persistent</span
                      >
                      <span
                        class="label label-<!-- IF items.hidable -->primary<!-- ELSE -->default<!-- ENDIF items.hidable -->"
                        >Hide</span
                      >
                    </div>
                    <div class="col-md-9">
                      <div class="pull-right">
                        <button
                          class="btn btn-primary item_update"
                          data-id="{items.whid}"
                        >
                          <i class="fa fa-pencil"></i> Edit</button
                        >&nbsp;&nbsp;&nbsp;
                        <button
                          class="btn btn-danger btn-default item_delete"
                          data-id="{items.whid}"
                        >
                          <i class="fa fa-trash"></i> Delete
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  class="panel-body"
                  style="color: {items.textcolor}; background-color: {items.bgcolor}"
                >
                  <div class="row">
                    <div class="col-md-9">
                      <strong>{items.name}</strong>
                      <p>{items.text}</p>
                    </div>
                  </div>
                </div>
                <div class="panel-footer">
                  <div>
                    <!-- IF items.groupsin_flag -->
                    <div>
                      <span class="label label-info"
                        >L'utente appartiene ai gruppi</span
                      >
                      <div>
                        <small
                          >&bull;
                          <!-- BEGIN items.groupsin -->@value &bull;<!-- END items.groupsin --></small
                        >
                      </div>
                    </div>
                    <!-- ENDIF items.groupsin_flag -->
                    <!-- IF items.groupsout_flag -->
                    <div>
                      <span class="label label-info"
                        >L'utente non appartiene ai gruppi</span
                      >
                      <div>
                        <small
                          >&bull;
                          <!-- BEGIN items.groupsout -->@value &bull;<!-- END items.groupsout --></small
                        >
                      </div>
                    </div>
                    <!-- ENDIF items.groupsout_flag -->
                    <!-- IF items.postnumber_flag -->
                    <div>
                      <span class="label label-info"
                        >L'utente ha scritto un n&deg; di post tra</span
                      >
                      <div>
                        <small
                          >{items.postnumber_min} e
                          {items.postnumber_max}</small
                        >
                      </div>
                    </div>
                    <!-- ENDIF items.postnumber_flag -->
                    <!-- IF items.notvisitedbymore_flag -->
                    <div>
                      <span class="label label-info"
                        >L'utente non ha visitato il forum da oltre</span
                      >
                      <div>
                        <small>{items.notvisitedbymore} giorni</small>
                      </div>
                    </div>
                    <!-- ENDIF items.notvisitedbymore_flag -->
                    <!-- IF items.username_flag -->
                    <div>
                      <span class="label label-info">L'utente è</span>
                      <div><small>{items.username}</small></div>
                    </div>
                    <!-- ENDIF items.username_flag -->
                    <!-- IF items.notlogged -->
                    <div>
                      <span class="label label-info"
                        >L'utente non è loggato</span
                      >
                    </div>
                    <!-- ENDIF items.notlogged -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </li>
        <!-- END items -->
      </ul>
    </div>
  </div>

  <button
    component="nodebb-plugin-welcome-header/add-header-btn"
    id="item_new"
    data-action="create"
    class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"
  >
    <i class="material-icons">add</i>
  </button>
</div>
