<div>
  <form role="form" id="connect_welcome_header_frm">
    <ul id="connect_welcome_header_tab" class="nav nav-tabs">
      <li class="active">
        <a
          href="#connect_welcome_header_tab_main"
          onclick="function tabPrevent(e) { e.preventDefault(); return false;}"
          data-toggle="tab"
          >Main</a
        >
      </li>
      <li>
        <a
          href="#connect_welcome_header_tab_conditions"
          onclick="function tabPrevent(e) { e.preventDefault(); return false;}"
          data-toggle="tab"
          >Conditions</a
        >
      </li>
    </ul>
    <div id="connect_welcome_header_tabContent" class="tab-content">
      <div class="tab-pane fade in active" id="connect_welcome_header_tab_main">
        <div class="form-group">
          <label for="connect_welcome_header_name">Name</label>
          <input
            type="text"
            id="connect_welcome_header_name"
            name="name"
            title="Name"
            class="form-control"
            placeholder=""
            value=""
          /><br />
        </div>
        <div class="form-group">
          <label for="connect_welcome_header_text">Text</label>
          <textarea
            id="connect_welcome_header_text"
            name="text"
            data-field="text"
            class="form-control"
            style="padding: 10px"
          ></textarea>
        </div>
        <div class="form-group">
          <label for="connect_welcome_header_bgcolor">Background Color</label>
          <input
            type="color"
            id="connect_welcome_header_bgcolor"
            name="bgcolor"
            data-field="bgcolor"
            placeholder="#0059b2"
            class="form-control"
            value=""
          />
        </div>
        <div class="form-group">
          <label for="connect_welcome_header_textcolor">Text Color</label>
          <input
            type="color"
            id="connect_welcome_header_textcolor"
            name="textcolor"
            data-field="textcolor"
            placeholder="#0059b2"
            class="form-control"
            value=""
          />
        </div>
        <div class="row">
          <div class="col-lg-4">
            <div class="checkbox">
              <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                <input
                  class="mdl-switch__input"
                  type="checkbox"
                  name="active"
                />
                <span class="mdl-switch__label"><strong>Active</strong></span>
              </label>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="checkbox">
              <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                <input
                  class="mdl-switch__input"
                  type="checkbox"
                  name="persistent"
                />
                <span class="mdl-switch__label"
                  ><strong>Persistent</strong></span
                >
              </label>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="checkbox">
              <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                <input
                  class="mdl-switch__input"
                  type="checkbox"
                  name="hidable"
                />
                <span class="mdl-switch__label"><strong>Hidable</strong></span>
              </label>
            </div>
          </div>
        </div>
      </div>

      <div class="tab-pane fade in" id="connect_welcome_header_tab_conditions">
        <div>
          <div class="checkbox">
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
              <input
                class="mdl-switch__input"
                type="checkbox"
                name="groupsin_flag"
              />
              <span class="mdl-switch__label"
                ><strong>L'utente appartiene ai gruppi</strong></span
              >
            </label>
          </div>
          <div class="form-group">
            <select
              multiple
              id="connect_welcome_header_groupsin"
              name="groupsin"
              name="groupsin"
              data-selected="{connect_welcome_header.groupsin}"
              class="form-control"
            >
              <!-- BEGIN groups -->
              <option value="@value">@value</option>
              <!-- END groups -->
            </select>
          </div>
        </div>
        <div>
          <div class="checkbox">
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
              <input class="mdl-switch__input" type="checkbox" />
              <span class="mdl-switch__label"
                ><strong>L'utente non appartiene ai gruppi</strong></span
              >
            </label>
          </div>
          <div class="form-group">
            <select
              multiple
              id="connect_welcome_header_groupsout"
              name="groupsout"
              data-field="groupsout"
              data-selected="{connect_welcome_header.groupsin}"
              class="form-control"
            >
              <!-- BEGIN groups -->
              <option value="@value">@value</option>
              <!-- END groups -->
            </select>
          </div>
        </div>
        <div>
          <div class="checkbox">
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
              <input
                class="mdl-switch__input"
                type="checkbox"
                name="postnumber_flag"
              />
              <span class="mdl-switch__label"
                ><strong
                  >L'utente ha scritto un n&deg; di post tra</strong
                ></span
              >
            </label>
          </div>
          <div class="row">
            <div class="col-lg-5 form-group">
              <input
                type="number"
                id="connect_welcome_header_postnumber_min"
                name="postnumber_min"
                value=""
                class="form-control"
              />
            </div>
            <div class="col-lg-2 text-center">e</div>
            <div class="col-lg-5 form-group">
              <input
                type="number"
                id="connect_welcome_header_postnumber_max"
                name="postnumber_max"
                value=""
                class="form-control"
              />
            </div>
          </div>
        </div>
        <div>
          <div class="checkbox">
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
              <input
                class="mdl-switch__input"
                type="checkbox"
                name="notvisitedbymore_flag"
              />
              <span class="mdl-switch__label"
                ><strong
                  >L'utente non ha visitato il forum da oltre</strong
                ></span
              >
            </label>
          </div>
          <div class="input-group form-group">
            <input
              type="number"
              id="connect_welcome_header_notvisitedbymore"
              name="notvisitedbymore"
              value=""
              class="form-control"
            />
            <div class="input-group-addon">giorni</div>
          </div>
        </div>
        <div>
          <div class="checkbox">
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
              <input
                class="mdl-switch__input"
                type="checkbox"
                name="username_flag"
              />
              <span class="mdl-switch__label"><strong>L'utente è</strong></span>
            </label>
          </div>
          <div class="form-group">
            <input
              type="text"
              id="connect_welcome_header_username"
              name="username"
              value=""
              class="form-control"
            />
          </div>
        </div>
        <div>
          <div class="checkbox">
            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
              <input
                class="mdl-switch__input"
                type="checkbox"
                name="notlogged"
              />
              <span class="mdl-switch__label"
                ><strong>L'utente non è loggato</strong></span
              >
            </label>
          </div>
        </div>
      </div>
    </div>
    <input
      id="connect_welcome_header_whid"
      name="whid"
      type="hidden"
      data-field="whid"
      value=""
    />
  </form>
</div>
