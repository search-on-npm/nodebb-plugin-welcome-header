<!-- IF items.length -->
<div id="welcome-header-widget">
  <!-- BEGIN items -->
  <div
    class="alert"
    style="color: {items.textcolor}; background-color: {items.bgcolor}"
  >
    <!-- IF items.hidable -->
    <button
      type="button"
      class="close"
      data-dismiss="alert"
      aria-label="Close"
      onclick="welcomeHeaderClose({items.whid});"
    >
      <span aria-hidden="true">&times;</span>
    </button>
    <!-- ENDIF items.hidable -->
    {items.text}
  </div>
  <!-- END items -->
</div>
<!-- ENDIF items.length -->
