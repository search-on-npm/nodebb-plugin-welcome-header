"use strict";

const controllers = require("./lib/controllers");
const api = require("./lib/api");
const sockets = require("./lib/sockets");
const db = require.main.require("./src/database");
const user = require.main.require("./src/user");
const groups = require.main.require("./src/groups");
const SocketPlugins = require.main.require("./src/socket.io/plugins");
const routeHelpers = require.main.require("./src/routes/helpers");
const plugin = {};

let app;

SocketPlugins.connectwelcomeheader = sockets;

plugin.init = async (params) => {
  app = params.app;
  routeHelpers.setupAdminPageRoute(
    params.router,
    "/admin/plugins/welcome-header",
    [],
    controllers.renderAdmin
  );
};

plugin.addRoutes = async ({ router, middleware }) => {
  const adminMiddlewares = [
    middleware.ensureLoggedIn,
    middleware.admin.checkPrivileges,
  ];

  routeHelpers.setupApiRoute(
    router,
    "get",
    "/welcome-headers",
    [],
    api.getHeaders
  );

  routeHelpers.setupApiRoute(
    router,
    "get",
    "/welcome-header/:whid",
    [],
    api.getHeader
  );

  routeHelpers.setupApiRoute(
    router,
    "put",
    "/welcome-header",
    adminMiddlewares,
    api.saveHeader
  );
  routeHelpers.setupApiRoute(
    router,
    "delete",
    "/welcome-header",
    adminMiddlewares,
    api.deleteHeader
  );
};

plugin.addAdminNavigation = async (header) => {
  header.plugins.push({
    route: "/plugins/welcome-header",
    icon: "fa-file-image-o",
    name: "Welcome Header",
  });
  return header;
};

plugin.defineWidgets = function (widgets, callback) {
  widgets = widgets.concat([
    {
      widget: "welcomeheader",
      name: "Welcome Header",
      description: "User Welcome Headers",
      content: "admin/widgets/welcomeheaderwidget.tpl",
    },
  ]);

  callback(null, widgets);
};

plugin.renderWelcomeHeaderWidget = async (widget) => {
  var start = 0;
  var stop = -1;
  var set = "connect-welcome-header";
  let response = [];

  const isAdmin = await user.isAdministrator(widget.uid);
  if (!isAdmin) {
    const welcome_header_ids = await db.getSortedSetRange(set, start, stop);
    const user_welcome_header_reader = await db.getObject(
      `${set}:whid:uid:${widget.uid}:readed`
    );

    if (!Array.isArray(welcome_header_ids) || !welcome_header_ids.length) {
      return null;
    }

    var keys = welcome_header_ids
      .filter(function (whid) {
        return (
          !user_welcome_header_reader ||
          !user_welcome_header_reader.array ||
          user_welcome_header_reader.array.indexOf(whid) === -1
        );
      })
      .map(function (whid) {
        return set + ":whid:" + whid;
      });
    const welcome_headers = await db.getObjects(keys);

    welcome_headers.forEach(async (header) => {
      let semaphore = false;
      console.log(header);
      if (header.active) {
        if (header.groupsin_flag && widget.uid && header.groupsin) {
          const is_member = await groups.isMember(widget.uid, header.groupsin);
          if (is_member) {
            response.push(header);
            semaphore = true;
          }
        } else if (header.groupsout_flag && widget.uid && header.groupsout) {
          const is_member = await groups.isMember(widget.uid, header.groupsout);
          if (!is_member) {
            response.push(header);
            semaphore = true;
          }
        } else if (header.notlogged && !widget.uid) {
          console.log("NOT LOGGED IN");
          response.push(header);
        } else if (header.postnumber_flag && widget.uid) {
          const user_postcount = await user.getUserField(
            widget.uid,
            "postcount"
          );
          if (
            user_postcount >= header.postnumber_min &&
            user_postcount <= header.postnumber_max
          ) {
            response.push(header);
            semaphore = true;
          }
        } else if (header.notvisitedbymore_flag && widget.uid) {
          const today = new Date().getTime();
          const timestamp_giorni = header.notvisitedbymore * 24 * 3600 * 1000;
          const user_lastonline = await user.getUserField(
            widget.uid,
            "lastonline"
          );
          if (user_lastonline + timestamp_giorni <= today) {
            response.push(header);
            semaphore = true;
          }
        } else if (header.username_flag && header.username && widget.uid) {
          var username_search = header.username.trim().toLowerCase();
          const username_uid = await user.getUidByUsername(username_search);
          if (username_uid === widget.uid) {
            response.push(header);
            semaphore = true;
          }
        }
        if (semaphore && !header.hidable && !header.persistent) {
          db.listAppend(`${set}:whid:uid:${widget.uid}:readed`, header.whid);
        }
      }
    });
  }
  if (response.length > 0) {
    const html = await app.renderAsync("widgets/welcomeheaderwidget.tpl", {
      items: response,
    });
    widget.html = html;
    return widget;
  }
  return "";
};

module.exports = plugin;
